"use strict"

const FeatureSwitches = {
  TAGS_ENABLED: true,
  HTML_TEMPLATES: true,
}

export default FeatureSwitches
